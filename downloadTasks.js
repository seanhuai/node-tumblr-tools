const config = require('./options');
const request = require('./request');
const fs = require('fs');

/*
 *  Class TaskList 任务组列表类
 *  
 *  push() 添加列表项目方法
 *    @param item:any 传入数据
 *    @returns number 返回列表长度  
 *  size() 返回列表长度方法
 *    @returns number/boolean 
 *  catch() 读取队列头部方法
 *  isEmpty() 判断列表为空方法
 *    @returns boolean
 */
class TaskList {
  constructor(){
    this.list = new Array();
  }
  size(){
    return this.list.length?this.list.length:false;
  }
  push(item){
    this.list.push(item);
    return this.list.length;
  }
  catch(){
    if(this.list.length > 0){
      return this.list.shift();
    }else {
      return false;
    }
  }
  isEmpty(){
    return this.list.length == 0;
  }
}

/*
 *  Class Task 任务组类
 *  
 *  createTask() 新建任务方法
 *    @returns number
 *  callTask() 调用首个任务方法
 *    @returns function|false
 *  callTasks() 并行调用任务方法
 *    @param num:number 并行调用任务数
 *    @returns null
 */
class Task {
  constructor(){
    this.list = new TaskList();
  }
  createTask(){
    const args = Array.from(arguments);
    this.list.push(args);
    return this.list.length;
  }
  callTask(){}
  callTasks(num){
    if(!num) return ;
    this.callTask();
    this.callTasks(num-1);
  }
}

/*
 *  Class RequestTasks 请求任务组类
 *  
 *  createTask() extend Task
 *  callTask() 调用请求任务方法
 *    @returns any|false
 */
class RequestTasks extends Task {
  callTask(){
    const item = this.list.catch();
    return item;
  }
  callTasks(){
    return false;
  }
}

/*
 *  Class DownloadTasks 下载任务组类
 *  
 *  createTask() extend Task
 *  callTask() 调用下载任务方法
 *    @returns function|false
 *  callTasks() extend Task  
 *  download() 实施下载流程方法
 *    @param item:array
 *           item[0]:下载内容地址
 *           item[1]:下载文件名，含后缀名
 *           item[2]:下载文件目录路径
 */
class DownloadTasks extends Task {
  constructor(){
    super();
    this.list = new TaskList();
    this.reDownloadList = new TaskList();
  }
  async callTask(){
    const item = this.list.catch();
    return item?await this.download(item):false; 
  }
  async download(item){
    if(!fs.existsSync(item[2])) fs.mkdirSync(item[2]);
    const filepath = `${item[2]}/${item[1]}`;
    if(!fs.existsSync(filepath)) {
      await request.get(item[0],{encoding: 'binary'}).then((res)=>{
        fs.writeFileSync(filepath,res,'binary');
        this.callTask();
      }).catch((err)=>{
        console.log(`下载 ${item[1]} 时出现错误 ${err}`);
        this.reDownloadList.push(item);
        this.callTask();        
      });
    }else{
      this.callTask();
    }
  }
}

module.exports = {DownloadTasks, RequestTasks};