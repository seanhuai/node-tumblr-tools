const fs = require('fs');
const config = require('./options');
const request = require('./request');
const { RequestTasks, DownloadTasks } = require('./downloadTasks');

/* Class Funcs 主函数类 */
class Funcs {
  constructor(options = {}) {
    // 下载任务组初始化
    this.downloadTasks = new DownloadTasks();
    // 页面任务组初始化
    this.requestTasks = new RequestTasks();
    // 设定默认设置
    const defaultOptions = {
      username: 'm3102', // 查询用户名
      mediatype: 'photo', // 查询内容类型
      page: 0, // 查询分页数，每页默认20项
      thread: config.download.thread // 同时启动的工作数
    }
    // 复写设置内容，使用默认内容补齐
    for (const key in defaultOptions) {
      if (!options.hasOwnProperty(key) || options[key] == undefined) options[key] = defaultOptions[key];
    }
    // 将设置内容赋予类
    this.options = options;
  }

  /* 
  *  createDownloadTask() 访问链接读取下载项内容，并建立下载任务
  *  @param url:string
  */
  async createDownloadTask(url) {
    // 请求 API 地址，获取内容列表
    await request.get(config.api.baseURL + url).then((res) => {
      // 格式化返回结果
      const json = JSON.parse(res);
      // 读取返回的每篇内容
      for (const key in json.response.posts) {
        const item = json.response.posts[key];
        // 判断请求媒体类型，分类处理链接
        if (this.options.mediatype == 'photo') {
          for (let i = 0; i < item.photos.length; i++) {
            // 获取指定内容的原始尺寸图片
            const url = item.photos[i].original_size.url;

            this.downloadTasks.createTask(url,
              this.getName(url, 'photo'),
              `${config.download.path}/${this.options.username}`);
          }
        } else if (this.options.mediatype == 'video') {
          if (item.video_type == 'tumblr') {
            const url = item.video_url;
            this.downloadTasks.createTask(url,
              this.getName(url, 'video'),
              `${config.download.path}/${this.options.username}`);
          }
        }
      }
    }).catch((err) => {
      console.log('获取图片地址列表时发生错误，请重试 ' + err);
    })
  }

  /* 
  *  getFile() 触发下载任务
  *  @param n:number 并发下载数
  */
  async getFile(n) {
    await this.downloadTasks.callTasks(n);
  }

  /* 
    getName() 根据媒体类型获取文件名
    @param url:string 
    @param mediatype:'photo'|'video'
  */
  getName(url, mediatype) {
    return mediatype == 'photo' ? url.split('/')[4] : url.split('/')[3];
  }

  /* getPageNumber() 获取内容页码数 */
  async getPageNumber() {
    if (this.options.page == 0) {
      const url = `/blog/${this.options.username}/posts/${this.options.mediatype}?api_key=${config.api.key}`;
      await request.get(config.api.baseURL + url).then((res) => {
        const json = JSON.parse(res);
        if (json.meta.status == 200) {
          this.options.page = Math.ceil(json.response.total_posts / 20);
        }
      }).catch((err) => {
        console.log('获取页码时发生错误，请重试 ' + err)
      })
    }
  }

  /* createRequestTask() 新建请求任务 */
  createRequestTask() {
    for (let page = this.options.page; page > 0; page--) {
      const url = `/blog/${this.options.username}/posts/${this.options.mediatype}?offset=${(page - 1) * 20}&api_key=${config.api.key}`;
      this.requestTasks.createTask(url)
    }
  }

  /* 
  *  callRequestTask() 调用请求任务组
  *  @param num
  */
  async callRequestTask(num) {
    if (num <= 0) return;
    await this.createDownloadTask(this.requestTasks.callTask())
    await this.callRequestTask(num - 1);
  }

  /* get() 主函数，依次调用其他功能函数 */
  async get() {
    await this.getPageNumber();
    await this.createRequestTask();
    console.log(`查询到 ${this.requestTasks.list.size()} 页内容`);
    await this.callRequestTask(this.requestTasks.list.size());
    console.log(`查询到 ${this.downloadTasks.list.size()} 项内容，开始下载`);
    await this.getFile(this.options.thread);
  }
}

module.exports = Funcs;