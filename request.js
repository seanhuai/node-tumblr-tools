const Request = require('request-promise-native');
const config = require('./options');

const request = Request.defaults({
  timeout: 5000,
  proxy: `http://${config.proxy.host}:${config.proxy.port}`
})

module.exports = request;